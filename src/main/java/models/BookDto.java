package models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represents a book. 
 * 
 * @author dom
 *
 */
public class BookDto {

  @Size(min = 5)
  public String title;

  @Size(min = 5)
  public String author;
  
  @NotNull
  public String topic;

  public BookDto(String title, String author, String topic) {
    this.title = title;
    this.author = author;
    this.topic = topic;
  }
  
  public BookDto() {
  }

}
