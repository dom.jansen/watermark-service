package models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An abstract base class for books and journals
 * 
 * @author dom
 */
public class DocumentDto {

  @NotNull
  public Long id;
  
  @Size(min = 5)
  public String title;

  @Size(min = 5)
  public String author;
  
  public String content;

  public WatermarkDto watermark;

  public DocumentDto(Long id, String title, String author, WatermarkDto watermark, String content) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.watermark = watermark;
    this.content = content;
  }
  
  public DocumentDto(){}
}
