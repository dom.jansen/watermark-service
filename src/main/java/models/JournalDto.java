package models;

import javax.validation.constraints.Size;

/**
 * Represents a journal.
 * 
 * @author dom
 *
 */
public class JournalDto {

  @Size(min = 5)
  public String title;

  @Size(min = 5)
  public String author;
  
  public JournalDto(String title, String author) {
    this.title = title;
    this.author = author;
  }

  public JournalDto() {
  }

}
