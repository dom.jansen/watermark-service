package models;

import javax.validation.constraints.Size;

/**
 * Represents a watermark
 * 
 * @author dom
 */
public class WatermarkDto {

  @Size(min = 5)
  public String title;

  @Size(min = 5)
  public String author;

  public String content;

  public WatermarkDto(String title, String author, String content) {
    this.title = title;
    this.author = author;
    this.content = content;
  }
  
  public WatermarkDto(){}
}
