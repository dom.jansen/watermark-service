package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * DB entity for the Documents
 * 
 * @author dom
 *
 */
@Entity
public class Document {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;

  String title;
  String content;
  String author;
  String topic;

  public Document() {
  }

  public Document(String title, String content, String author, String topic) {
    this.title = title;
    this.content = content;
    this.author = author;
    this.topic = topic;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }
}
