package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * DB entity for the Documents
 * 
 * @author dom
 *
 */
@Entity
public class Watermark {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;

  private Long document;

  public Watermark() {
  }

  public Watermark(Long document) {
    this.setDocument(document);
  }

  public Long getId() {
    return id;
  }

  public Long getDocument() {
    return document;
  }

  public void setDocument(Long document) {
    this.document = document;
  }
}
