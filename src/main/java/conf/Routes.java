/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package conf;

import com.google.inject.Inject;

import controllers.DocumentController;
import ninja.Router;
import ninja.application.ApplicationRoutes;
import ninja.utils.NinjaProperties;

public class Routes implements ApplicationRoutes {

  @Inject
  NinjaProperties ninjaProperties;

  /**
   * Using a (almost) nice DSL we can configure the router.
   * 
   * The second argument NinjaModuleDemoRouter contains all routes of a
   * submodule. By simply injecting it we activate the routes.
   * 
   * @param router
   *            The default router of this application
   */
  @Override
  public void init(Router router) {

    ///////////////////////////////////////////////////////////////////////
    // Bootstrapping /FIXTURES
    ///////////////////////////////////////////////////////////////////////
    if (!ninjaProperties.isProd()) {
      router.GET().route("/setup").with(DocumentController.class, "setup");
    }

    ///////////////////////////////////////////////////////////////////////
    // Documents
    ///////////////////////////////////////////////////////////////////////
    router.GET().route("/api/document").with(DocumentController.class, "getAllDocuments");

    router.POST().route("/api/journal").with(DocumentController.class, "postJournal");
    router.POST().route("/api/book").with(DocumentController.class, "postBook");
    router.GET().route("/api/watermark/{id}").with(DocumentController.class, "getWatermark");
    
  }

}
