package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import etc.Content;
import models.BookDto;
import models.Document;
import models.JournalDto;
import ninja.jpa.UnitOfWork;

/**
 * DAO for accessing {@link Document} table
 * 
 * @author dom
 *
 */
public class DocumentDao {

  @Inject
  Provider<EntityManager> entitiyManagerProvider;

  /**
   * Get all Documents from the database
   * @return
   */
  @UnitOfWork
  public List<Document> getDocuments() {

    EntityManager entityManager = entitiyManagerProvider.get();

    TypedQuery<Document> query = entityManager.createQuery("SELECT x FROM Document x", Document.class);
    List<Document> documents = query.getResultList();

    return documents;

  }

  @Transactional
  public Document createJournal(JournalDto journalDto) {
    EntityManager entityManager = entitiyManagerProvider.get();
    Document document = new Document(journalDto.title, Content.JOURNAL.getValue(), journalDto.author, null);

    entityManager.persist(document);
    return document;
  }

  @Transactional
  public Document createBook(BookDto bookDto) {
    EntityManager entityManager = entitiyManagerProvider.get();
    Document document = new Document(bookDto.title, Content.BOOK.getValue(), bookDto.author, bookDto.topic);

    entityManager.persist(document);
    return document;
  }


  @UnitOfWork
  public Document getDocumentById(Long id) {
    EntityManager entityManager = entitiyManagerProvider.get();

    TypedQuery<Document> query = entityManager.createQuery("SELECT x FROM Document x WHERE x.id = :id",
        Document.class);
    query.setParameter("id", id);

    // as getSingleResult throws an exception if no result is found, lets go the dirty way
    List<Document> document = query.getResultList();

    if (document == null || document.isEmpty()) {
      return null;
    }

    return document.get(0);
  }

  @UnitOfWork
  public Document getBookByName(BookDto bookDto) {
    EntityManager entityManager = entitiyManagerProvider.get();

    TypedQuery<Document> query = entityManager.createQuery(
        "SELECT x FROM Document x WHERE x.title = :title AND x.author = :author AND x.content = :content AND x.topic = :topic ",
        Document.class);
    query.setParameter("title", bookDto.title).setParameter("author", bookDto.author).setParameter("content",
        Content.BOOK.getValue()).setParameter("topic", bookDto.topic.toLowerCase());

    // as getSingleResult throws an exception if no result is found, lets go the dirty way
    List<Document> document = query.getResultList();

    if (document == null || document.size() == 0) {
      return null;
    }
    return document.get(0);
  }

  @UnitOfWork
  public Document getJournalByName(JournalDto journalDto) {

    EntityManager entityManager = entitiyManagerProvider.get();

    TypedQuery<Document> query = entityManager.createQuery(
        "SELECT x FROM Document x WHERE x.title = :title AND x.author = :author AND x.content = :content AND x.topic is null ",
        Document.class);
    query.setParameter("title", journalDto.title).setParameter("author", journalDto.author).setParameter(
        "content", Content.JOURNAL.getValue());

    // as getSingleResult throws an exception if no result is found, lets go the dirty way
    List<Document> document = query.getResultList();

    if (document == null || document.size() == 0) {
      return null;
    }
    return document.get(0);

  }

}
