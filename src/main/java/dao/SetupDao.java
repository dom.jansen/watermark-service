package dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import models.Document;
import models.Watermark;


/**
 * For Test Fixtures
 * 
 * @author dom
 *
 */
public class SetupDao {
    
    @Inject
    Provider<EntityManager> entityManagerProvider;

    @Transactional
    public void setup() {
        
        EntityManager entityManager = entityManagerProvider.get();
        
        TypedQuery<Document> q = entityManager.createQuery("SELECT x FROM Document x", Document.class);
        List<Document> documents = q.getResultList();        
        
        if (documents.size() == 0) {

            // Create a new documents and save it
            Document doc1 = new Document("The Dark Code", "book", "Bruce Wayne", "science");
            entityManager.persist(doc1);
            Document doc2 = new Document("How to make money", "book", "Dr. Evil", "business");
            entityManager.persist(doc2);
            Document doc3 = new Document("Journal of human flight routes", "journal", "Clark Kent", null);
            entityManager.persist(doc3);
            

            entityManager.setFlushMode(FlushModeType.COMMIT);
            entityManager.flush();
            
            //create watermarks for this documents
            Watermark water1 = new Watermark(doc1.getId());
            entityManager.persist(water1);
            Watermark water2 = new Watermark(doc2.getId());
            entityManager.persist(water2);
            Watermark water3 = new Watermark(doc3.getId());
            entityManager.persist(water3);
            entityManager.flush();
        }

    }
}
