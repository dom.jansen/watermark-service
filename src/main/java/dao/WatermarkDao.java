package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import models.Document;
import models.Watermark;
import ninja.jpa.UnitOfWork;

/**
 * DAO for accessing {@link Watermark} table
 * 
 * @author dom
 *
 */
public class WatermarkDao {

  @Inject
  Provider<EntityManager> entitiyManagerProvider;

 

  @Transactional
  public void createWatermark(Document document) {
    EntityManager entityManager = entitiyManagerProvider.get();
    Watermark watermark = new Watermark(document.getId());

    entityManager.persist(watermark);
  }

  @UnitOfWork
  public Watermark getWatermarkById(Long id) {

    EntityManager entityManager = entitiyManagerProvider.get();

    TypedQuery<Watermark> query = entityManager.createQuery("SELECT x FROM Watermark x WHERE x.id = :id",
        Watermark.class);
    query.setParameter("id", id);

    // as getSingleResult throws an exception if no result is found, lets go the dirty way
    List<Watermark> watermark = query.getResultList();

    if (watermark == null || watermark.isEmpty()) {
      return null;
    }

    return watermark.get(0);

  }

}
