package etc;

/**
 * Some simple real name http status codes
 * @author dom
 *
 */
public class StatusConstants {
  public static final String SC_201 = "Created";
  public static final String SC_404 = "Not found";
  public static final String SC_400 = "Bad Request";
  public static final String SC_400_ALREADY_IN_DB = "Already in database";
  public static final String SC_400_BAD_TOPIC = "Unknown topic";
}
