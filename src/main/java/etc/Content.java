package etc;

/**
 * Enum representing type of documents 
 * @author dom
 * 
 */
public enum Content {
  JOURNAL("journal"), BOOK("book");

  private final String value;

  private Content(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

}
