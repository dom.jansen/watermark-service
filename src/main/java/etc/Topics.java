package etc;

import models.BookDto;

/**
 * Enum representing Topics of {@link BookDto}  
 * @author dom
 * 
 */
public enum Topics {
  BUSINESS("business"), SCIENCE("science"), MEDIA("media");
  
  private final String value;

  private Topics(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
  
  /**
   * Returns enum representing the given value. Otherwise returns null. 
   */
  public static Topics getEnum(String value) {
    for (Topics topics : Topics.values()) {
      if (topics.getValue().equals(value.toLowerCase())) {
        return topics;
      }
    }
    return null;
  }

}
