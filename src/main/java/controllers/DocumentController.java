package controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.DocumentDao;
import dao.SetupDao;
import dao.WatermarkDao;
import etc.StatusConstants;
import etc.Topics;
import models.BookDto;
import models.Document;
import models.DocumentDto;
import models.JournalDto;
import models.Watermark;
import models.WatermarkDto;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;
import ninja.utils.NinjaProperties;
import thread.WatermarkThread;

/**
 * Contains logic for posting books and journals that should get a watermark
 * 
 * @author dom
 *
 */
@Singleton
public class DocumentController {

  @Inject
  DocumentDao documentDao;
  @Inject
  WatermarkDao watermarkDao;
  @Inject
  SetupDao setupDao;
  @Inject
  NinjaProperties ninjaProperties;

  
  ///////////////////////////////////////////////////////////////////////////
  // Method to put initial data in the db...
  ///////////////////////////////////////////////////////////////////////////
  public Result setup() {
      setupDao.setup();
      return  Results.json().render(StatusConstants.SC_201).status(201);
  }
  
  ///////////////////////////////////////////////////////////////////////////
  // Get all documents
  ///////////////////////////////////////////////////////////////////////////
  public Result getAllDocuments() {
    List<Document> documents = documentDao.getDocuments();
    List<DocumentDto> documentDtos = new ArrayList<DocumentDto>();

    for (Document doc : documents) {
      Watermark watermark = watermarkDao.getWatermarkById(doc.getId());
      documentDtos.add(createDto(doc, watermark != null));
    }
    return Results.json().render(documentDtos).status(200);

  }

  ///////////////////////////////////////////////////////////////////////////
  // Get watermark by document id
  ///////////////////////////////////////////////////////////////////////////
  public Result getWatermark(@PathParam("id") Long id) {

    Watermark watermark = watermarkDao.getWatermarkById(id);
    if (watermark != null) {
      Document document = documentDao.getDocumentById(watermark.getDocument());
      // create dto for the watermark
      DocumentDto documentDto = createDto(document, true);
      return Results.json().status(200).render(documentDto);
    } else {
      return Results.json().status(404).render(StatusConstants.SC_404);
    }

  }

  private DocumentDto createDto(Document document, boolean hasWatermark) {
    WatermarkDto watermarkDto = null;
    if (hasWatermark) {
      watermarkDto = new WatermarkDto(document.getTitle(), document.getAuthor(), document.getContent());

    }
    DocumentDto documentDto = new DocumentDto(document.getId(), document.getTitle(), document.getAuthor(),
        watermarkDto, document.getContent());
    return documentDto;
  }

  ///////////////////////////////////////////////////////////////////////////
  // Create new Journal
  ///////////////////////////////////////////////////////////////////////////
  public Result postJournal(JournalDto journalDto) {
    // return a 400...
    if (journalDto == null) {
      return Results.json().status(400).render(StatusConstants.SC_400);
    }

    // check if we have already this journal in our database
    Document document = documentDao.getJournalByName(journalDto);
    if (document != null) {
      return Results.json().status(400).render(StatusConstants.SC_400_ALREADY_IN_DB);
    }

    // else create journal
    Document createJournal = documentDao.createJournal(journalDto);

    // now create watermark for this journal
    new WatermarkThread(watermarkDao,createJournal, ninjaProperties).start();

    // return the document id for polling
    return Results.json().status(201).render(createJournal.getId());
  }

  ///////////////////////////////////////////////////////////////////////////
  // Create new Book
  ///////////////////////////////////////////////////////////////////////////
  public Result postBook(BookDto bookDto) {
    // return a 400...
    if (bookDto == null) {
      return Results.json().status(400).render(StatusConstants.SC_400);
    }
    
    //check if topic is valid
    if (Topics.getEnum(bookDto.topic) == null) {
      return Results.json().status(400).render(StatusConstants.SC_400_BAD_TOPIC);
    }

    // check if we have already this journal in our database
    Document document = documentDao.getBookByName(bookDto);
    if (document != null) {
      // check if an watermark exists
      return Results.json().status(400).render(StatusConstants.SC_400_ALREADY_IN_DB);
    }

    // else create journal
    Document createBook = documentDao.createBook(bookDto);

    // now create watermark for this journal
    new WatermarkThread(watermarkDao, createBook, ninjaProperties).start();

    // return the document id for polling
    return Results.json().status(201).render(createBook.getId());

  }

}
