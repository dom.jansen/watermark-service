package thread;

import dao.WatermarkDao;
import models.Document;
import ninja.utils.NinjaProperties;

/**
 * Thread for creating the watermark. The thread sleeps for {@value ninjaProperties.get("threadSleepTime")}ms 
 * to simulate asynchronous functionality.
 * 
 * Based on https://docs.oracle.com/javase/tutorial/essential/concurrency/runthread.html
 * 
 * @author dom
 *
 */
public class WatermarkThread extends Thread {

  private Document document;
  private WatermarkDao watermarkDao;
  private NinjaProperties ninjaProperties;

  public WatermarkThread(WatermarkDao watermarkDao, Document document, NinjaProperties ninjaProperties) {
    this.watermarkDao = watermarkDao;
    this.document = document;
    this.ninjaProperties = ninjaProperties;
  }

  public void run() {
    try {
      WatermarkThread.sleep(Integer.valueOf(ninjaProperties.get("threadSleepTime")));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    watermarkDao.createWatermark(document);
  }

}
