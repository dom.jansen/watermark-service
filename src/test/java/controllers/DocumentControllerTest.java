package controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import etc.StatusConstants;
import etc.Topics;
import models.BookDto;
import models.DocumentDto;
import models.JournalDto;
import ninja.NinjaTest;

/**
 * Test for {@link DocumentController}
 * @author dom
 *
 */
public class DocumentControllerTest extends NinjaTest {

  // /////////////////////////////////////////////////////////////////////
  // Test initial data:
  // /////////////////////////////////////////////////////////////////////
  @Test
  public void testGetAllDocuments() throws Exception {

    String response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/document");

    Type listTypeDocuments = new TypeToken<ArrayList<DocumentDto>>() {
    }.getType();
    List<DocumentDto> documents = new Gson().fromJson(response, listTypeDocuments);

    assertEquals(3, documents.size());
  }

  // /////////////////////////////////////////////////////////////////////
  // Post new journal:
  // /////////////////////////////////////////////////////////////////////
  @Test
  public void postJournal() throws Exception {
    // posting a bad json
    String response = ninjaTestBrowser.postJson(getServerAddress() + "api/journal", null);
    assertEquals("\""+StatusConstants.SC_400+"\"", response);

    // posting a journal that is already contained
    JournalDto dto = new JournalDto("Journal of human flight routes", "Clark Kent");
    response = ninjaTestBrowser.postJson(getServerAddress() + "api/journal", dto);
    assertEquals("\""+StatusConstants.SC_400_ALREADY_IN_DB+"\"", response);

    // posting a new journal
    dto = new JournalDto("title", "author");
    response = ninjaTestBrowser.postJson(getServerAddress() + "api/journal", dto);

    String id = response;
    assertEquals("4", id);
    
    //check if we now have 4 items in the db
    response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/document");

    Type listTypeDocuments = new TypeToken<ArrayList<DocumentDto>>() {
    }.getType();
    List<DocumentDto> documents = new Gson().fromJson(response, listTypeDocuments);

    assertEquals(4, documents.size());
    
    // poll with this id
    response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/watermark/"+id);
    assertEquals("\""+StatusConstants.SC_404+"\"", response);
    
    // wait until thread is finished
    System.out.println("Falling asleep");
    Thread.sleep(6000);
    
    
    System.out.println("Woke up");
    response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/watermark/"+id);
    
    DocumentDto document = new Gson().fromJson(response, DocumentDto.class);
    assertNotNull(document.watermark);
  }
  
  // /////////////////////////////////////////////////////////////////////
  // Post new book:
  // /////////////////////////////////////////////////////////////////////
  @Test
  public void postBook() throws Exception {
    // posting a bad json
    String response = ninjaTestBrowser.postJson(getServerAddress() + "api/book", null);
    assertEquals("\""+StatusConstants.SC_400+"\"", response);
    
    // posting a book that is already contained
    BookDto dto = new BookDto("The Dark Code", "Bruce Wayne", Topics.SCIENCE.getValue());
    response = ninjaTestBrowser.postJson(getServerAddress() + "api/book", dto);
    assertEquals("\""+StatusConstants.SC_400_ALREADY_IN_DB+"\"", response);

    // posting a book with not supported topic
    dto = new BookDto("The Dark Code", "Batman", "nature");
    response = ninjaTestBrowser.postJson(getServerAddress() + "api/book", dto);
    assertEquals("\""+StatusConstants.SC_400_BAD_TOPIC+"\"", response);
    
    // posting a new book
    dto = new BookDto("title", "author", Topics.MEDIA.getValue());
    response = ninjaTestBrowser.postJson(getServerAddress() + "api/book", dto);
    
    String id = response;
    assertEquals("4", id);
    
    //check if we now have 4 items in the db
    response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/document");

    Type listTypeDocuments = new TypeToken<ArrayList<DocumentDto>>() {
    }.getType();
    List<DocumentDto> documents = new Gson().fromJson(response, listTypeDocuments);

    assertEquals(4, documents.size());
    
    // poll with this id
    response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/watermark/"+id);
    assertEquals("\""+StatusConstants.SC_404+"\"", response);
    
    // wait until thread is finished
    System.out.println("Falling asleep");
    Thread.sleep(6000);
    
    System.out.println("Woke up");
    response = ninjaTestBrowser.makeRequest(getServerAddress() + "api/watermark/"+id);
    
    DocumentDto document = new Gson().fromJson(response, DocumentDto.class);
    assertNotNull(document.watermark);
  }

}
