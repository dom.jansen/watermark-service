package controllers;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import dao.DocumentDao;
import dao.SetupDao;
import dao.WatermarkDao;
import etc.Topics;
import models.BookDto;
import models.Document;
import models.JournalDto;
import models.Watermark;
import ninja.Result;

/**
 * Mocked unit tests for {@link DocumentController}
 * @author dom
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DocumentControllerMockTest {

  private static final String AUTHOR = "author";
  private static final String CONTENT = "content";
  private static final Long DOCUMENT_ID = 4L;
  private static final String TITLE = "title";
  @Mock
  WatermarkDao watermarkDao;
  @Mock
  DocumentDao documentDao;
  @Mock
  SetupDao setupDao;

  DocumentController documentController;
  List<Document> documents;
  private Document document;
  private Watermark watermark;

  @Before
  public void setupTest() {
    document = new Document();
    document.setId(DOCUMENT_ID);
    document.setTitle(TITLE);
    document.setContent(CONTENT);
    document.setAuthor(AUTHOR);
    documents = new ArrayList<Document>();
    documents.add(document);

    watermark = new Watermark();
    watermark.setDocument(DOCUMENT_ID);

    documentController = new DocumentController();
    documentController.documentDao = documentDao;
    documentController.watermarkDao = watermarkDao;
    documentController.setupDao = setupDao;

  }

  @Test
  public void testSetup() {
    Result result = documentController.setup();

    assertEquals(201, result.getStatusCode());

  }

  @Test
  public void getAllDocuments() {
    when(documentDao.getDocuments()).thenReturn(documents);
    when(watermarkDao.getWatermarkById(DOCUMENT_ID)).thenReturn(watermark);

    Result result = documentController.getAllDocuments();
    assertEquals(200, result.getStatusCode());

  }

  @Test
  public void getWatermark_404() {

    when(watermarkDao.getWatermarkById(DOCUMENT_ID)).thenReturn(null);

    Result result = documentController.getWatermark(DOCUMENT_ID);
    assertEquals(404, result.getStatusCode());
  }

  @Test
  public void getWatermark_200() {

    when(watermarkDao.getWatermarkById(DOCUMENT_ID)).thenReturn(watermark);
    when(documentDao.getDocumentById(DOCUMENT_ID)).thenReturn(document);

    Result result = documentController.getWatermark(DOCUMENT_ID);
    assertEquals(200, result.getStatusCode());

    verify(documentDao).getDocumentById(DOCUMENT_ID);
  }

  @Test
  public void postJournal() {

    // Journal is null
    Result result = documentController.postJournal(null);
    assertEquals(400, result.getStatusCode());

    // check if we have already this journal in our database
    JournalDto journalDto = new JournalDto(TITLE, AUTHOR);
    when(documentDao.getJournalByName(journalDto)).thenReturn(document);
    result = documentController.postJournal(journalDto);
    assertEquals(400, result.getStatusCode());

  }

  @Test
  public void postBook() {

    // Book is null
    Result result = documentController.postBook(null);
    assertEquals(400, result.getStatusCode());

    // check if we have already this journal in our database
    BookDto bookDto = new BookDto(TITLE, AUTHOR, Topics.MEDIA.getValue());
    when(documentDao.getBookByName(bookDto)).thenReturn(document);
    result = documentController.postBook(bookDto);
    assertEquals(400, result.getStatusCode());

    // bad topic
    bookDto = new BookDto(TITLE, AUTHOR, "nature");
    result = documentController.postBook(bookDto);
    assertEquals(400, result.getStatusCode());
  }
}
