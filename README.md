#Watermark-Test

A global publishing company that publishes books and journals wants to develop a service to watermark their documents. Book publications include topics in business, science and media. Journals don’t include any specific topics. A document (books, journals) has a title, author and a watermark property. An empty watermark property indicates that the document has not been watermarked yet.

The watermark service has to be asynchronous. For a given content document the service should return a ticket, which can be used to poll the status of processing. If the watermarking is finished the document can be retrieved with the ticket. The watermark of a book or a journal is identified by setting the watermark property of the object. For a book the watermark includes the properties content, title, author and topic. The journal watermark includes the content, title and author. 

Examples for watermarks:

```
#!json

{content:”book”, title:”The Dark Code”, author:”Bruce Wayne”, topic:”Science”}
```
```
#!json

{content:”book”, title:”How to make money”, author:”Dr. Evil”, topic:”Business”}
```
```
#!json

{content:”journal”, title:”Journal of human flight routes”, author:”Clark Kent”}
```


Tasks:

* Create an appropriate object-oriented model for the problem.
* Implement the Watermark-Service, meeting the above conditions.
* Provide Unit-Tests to ensure the functionality of the service.

#Implementation
This service is build with the [ninja framework](http://www.ninjaframework.org). 
##Why ninja?
ninja is is simple easy to use fullstack java framework. It supports maven archetypes to build sample projects with a build-in h3 database and hibernate support - everything we need for this service.
Maven, Guice, Eclipse Support, etc everything is available.
And of course, I'm a Java developer and there is not much time left to finish the project ;)

##Setup
###Database
We use the build-in h2 database. There are two tables. The used maven-archetype has a db migration functionality which we can abuse for creating the tables:


```
#!mysql

create table Document (
    id bigint generated by default as identity,
    content varchar(10) not null,
    topic varchar(10),
    title varchar(255) not null,
    author varchar(255) not null,
    primary key (id)
   
);

create table Watermark (
    id bigint generated by default as identity,
	document bigint,
    primary key (id),
    FOREIGN KEY (document) 
    REFERENCES public.Document(id)
);
```

The table "Document" contains all information about the documents.
The table "Watermark" contains all information about the available watermarks. Each watermark has only the foreign key for the related document that has all information for the watermark to avoid data duplication.
 

###Routes.java
Here we define the different routes we use for the service and map the route to a controller method:

```
#!java

router.GET().route("/setup").with(DocumentController.class, "setup");
router.GET().route("/api/document").with(DocumentController.class, "getAllDocuments");
router.POST().route("/api/journal").with(DocumentController.class, "postJournal");
router.POST().route("/api/book").with(DocumentController.class, "postBook");
router.GET().route("/api/watermark/{id}").with(DocumentController.class, "getWatermark");
```

* The "setup" route creates some dummy data for the database.
* "getAllDocuments" returns a list of all documents in the database.

Both endpoints are not required for the service, but they were really helpful for testing while developing the functionality.

###Controller
There is one Controller defined DocumentController. Here lives the business logic.
To create the watermark a thread is used to allow asynchronous behavior. In the "application.conf" the sleeping time is defined how long it takes until the watermark is created. 
In a first step I used a random time, but for testing it's easier to use a fixed value.

###Dao
For both Database tables we have a data acccess object.
The communicate with the database.

###Dto
The Data from the backend is converted to the Dto. We have a BookDto and and JournalDto for posting and a general DocumentDto for retrieving Documents from the Backend. 
With jackson annotations like @JsonTypeInfo it is possible to combine the dtos, but i was not able to get it working in ninja. The dtos where null when i tried to post them to the controller.

#How to run the service:

##Starting the service:

```
#!console

$mvn ninja:run
```

This starts the service on ```http://localhost:8080```.

##Testing 
There are two different kind of unit tests for controller where the logic lives: 

* real requests on a database
* mock tests

Run the tests:

```
#!console

$mvn test
```

##Create dummy data:

```
#!commandline
$curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/setup
```

Result: 201 Created

##List all Documents:

```
#!commandline
$curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/api/document
```

Result: 200 OK

```
#!json

[
  {
    "id":1,
    "title":"The Dark Code",
    "author":"Bruce Wayne",
    "content":"book",
    "watermark":{
      "title":"The Dark Code",
      "author":"Bruce Wayne",
      "content":"book"
    }
  },
  {
    "id":2,
    "title":"How to make money",
    "author":"Dr. Evil",
    "content":"book",
    "watermark":{
      "title":"How to make money",
      "author":"Dr. Evil",
      "content":"book"
    }
  },
  {
    "id":3,
    "title":"Journal of human flight routes",
    "author":"Clark Kent",
    "content":"journal",
    "watermark":null
  }
]
```
For the document with id 3 there is no watermark created yet.


##Post new Journal:

```
#!commandline

$curl -H "Content-Type: application/json" -X POST -d '{"title":"yourTitle","author":"authorName"}' http://localhost:8080/api/journal
```

Result: ID of Document which can be used for polling.
If Book is already in DB we get an error.

##Post new Book:

```
#!commandline

$curl -H "Content-Type: application/json" -X POST -d '{"title":"yourTitle","author":"authorName", "topic":{science,business,media}}' http://localhost:8080/api/book
```

Result: ID of Document which can be used for polling.
If Book is already in DB we get an error.

##Poll document

```
#!commandline

$curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/api/watermark/{document_id}
```

Result: 
If the watermark does not exist, an error is returned, otherwise the Document json.

```
#!json

{
  "id":7,
  "title":"yourTitle",
  "author":"authfeweworName",
  "content":"journal",
  "watermark":{
    "title":"yourTitle",
    "author":"authfeweworName",
    "content":"journal"
  }
}
```